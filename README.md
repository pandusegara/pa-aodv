# Priority Aware AODV

* NAMA    : AKBAR PANDU SEGARA
* NRP     : 05111850010009
* KELAS   : TD DESAIN AUDIT JARINGAN

# Info Paper

* Judul Paper   : Quality of Service Enhancement of Mobile Adhoc Networks Using Priority Aware Mechanism in AODV Protocol
* Sumber        : https://link.springer.com/article/10.1007/s11277-017-4453-3
* Penulis       : Jayson Keerthy Jayabarathan, A. Sivanantharaja, S. Robinson

# Konsep Paper

* Menerapkan Priority Aware pada protokol AODV. 
Pada AODV saat terjadi congestion pada jaringan, paket dengan prioritas rendah akan dikirimkan lebih dulu sehingga paket dengan prioritas rendah akan rawan terjadi paket drop. Prioritas ini didefinisikan secara eksternal oleh user (application based). Dalam penerapan prioritas ini menggunakan mekanisme rate cutting berdasarkan threshold value, priority information, dan transmission rate. Dimana paket yang memiliki prioritas yang lebih tinggi akan dikirim terlebih dahulu, prioritas di tentukan berdasarkan flow ID dan data rate pada tiap-tiap connection




